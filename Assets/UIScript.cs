﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UIScript : MonoBehaviour
{
    private string race;
    private void Start()
    {
        race = "race";
    }

    // Start is called before the first frame update
    public void undeadClick()
    {
        PlayerPrefs.SetString(race, "Undead");
    }

    public void humanClick()
    {
        PlayerPrefs.SetString(race, "Human");
    }
    
    public void goblinClick()
    {
        PlayerPrefs.SetString(race, "Goblin");
    }

    public void loadScene()
    {
        SceneManager.LoadScene(1);
    }
}
