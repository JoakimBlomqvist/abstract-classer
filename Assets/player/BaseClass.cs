﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BaseClass
{
   protected int hitPoints = 10;
   protected int damage = 2;
   protected int armor = 5;
   protected float speed = 6f;
   public abstract void Health(PlayerSpawn player);
   
   public abstract void Damage(PlayerSpawn player);

   public abstract void Armor(PlayerSpawn player);

   public abstract void Speed(PlayerSpawn player);

}
