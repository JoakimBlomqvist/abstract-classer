﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Goblin : BaseClass
{
    public override void Health(PlayerSpawn player)
    {
        player.health = hitPoints -= 5;
    }

    public override void Damage(PlayerSpawn player)
    {
        player.damage = damage += 5;
    }

    public override void Armor(PlayerSpawn player)
    {
        player.armor = armor -= 3;
    }

    public override void Speed(PlayerSpawn player)
    {
        player.speed = speed += 7f;
    }
}
