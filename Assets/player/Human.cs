﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Human : BaseClass
{
    public override void Health(PlayerSpawn player)
    {
       player.health = hitPoints + 2;
    }

    public override void Damage(PlayerSpawn player)
    {
        player.damage = damage;
    }

    public override void Armor(PlayerSpawn player)
    {
        player.armor = armor + 1;
    }

    public override void Speed(PlayerSpawn player)
    {
        player.speed = speed += 1f;
    }
}
