﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
   
    [SerializeField]private Rigidbody rb;
    [SerializeField]private PlayerSpawn player;
    
    [SerializeField]private int health = 0;

    [SerializeField]private int damage = 0;

    [SerializeField]private int armor = 0;
    
    [SerializeField]private float speed = 0f;

    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
        player = FindObjectOfType<PlayerSpawn>();

        health = player.health;
        damage = player.damage;
        armor = player.armor;
        speed = player.speed;
    }

    private void FixedUpdate()
    {
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(horizontal, 0, vertical);

        movement = movement.normalized * (speed * Time.deltaTime);
        
       rb.MovePosition(transform.position + movement);

    }
}
