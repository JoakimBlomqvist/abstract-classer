﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSpawn : MonoBehaviour
{
   
    private Human humanRace;
    private Undead undeadRace;
    private Goblin goblinRace;
    [SerializeField]private GameObject prefab;

    private string race;

    public float speed = 0f;
    
    public int health = 0;

    public int damage = 0;

    public int armor = 0;
    // Start is called before the first frame update
    private void Awake()
    {
        humanRace = new Human();
        undeadRace = new Undead();
        goblinRace = new Goblin();
        

        race = PlayerPrefs.GetString("race");
        Debug.Log(race);
        if (race == "Human")
        {
            humanRace.Health(this);
            humanRace.Damage(this);
            humanRace.Armor(this);
            humanRace.Speed(this);
        }
        
        if (race == "Undead")
        {
            undeadRace.Health(this);
            undeadRace.Damage(this);
            undeadRace.Armor(this);
            undeadRace.Speed(this);
        }
        
        if (race == "Goblin")
        {
            goblinRace.Health(this);
            goblinRace.Damage(this);
            goblinRace.Armor(this);
            goblinRace.Speed(this);
        }

        Instantiate(prefab, transform.position, Quaternion.identity);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
