﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Undead : BaseClass
{
    public override void Health(PlayerSpawn player)
    {
       player.health = hitPoints;
    }

    public override void Damage(PlayerSpawn player)
    {
       player.damage = damage + 4;
    }

    public override void Armor(PlayerSpawn player)
    {
        player.armor = armor;
    }

    public override void Speed(PlayerSpawn player)
    {
        player.speed = speed += 2f;
    }
}
